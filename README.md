# pulsar

#### 介绍
Apache Pulsar 是一个企业级的发布订阅（pub-sub）消息系统。向Pulsar发送数据的应用程序叫做生产者（producer），而从Pulsar读取数据的应用程序叫做消费者（consumer）。有时候消费者也被叫作订阅者。主题（topic）是Pulsar的核心资源，一个主题可以被看成是一个通道，消费者向这个通道发送数据，消费者从这个通道拉取数据。

#### 软件架构
Apache Pulsar 集群由两层组成：
无状态服务层，由一组接收和传递消息的 Broker 组成
有状态存储层，由一组名为 bookies 的 Apache BookKeeper 存储节点组成，可持久化地存储消息
通过分层架构实现了存储和计算分离
另外使用Apache Zookeeper进行元数据管理


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
