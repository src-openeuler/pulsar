%define debug_package %{nil}
%define pulsar_ver 2.10.6
%define pkg_ver 3
%define _prefix /opt/pulsar
Summary: Cloud-Native, Distributed Messaging and Streaming
Name: pulsar
Version: %{pulsar_ver}
Release: %{pkg_ver}
License: Apache-2.0
Group: Applications/Message
URL: https://pulsar.apache.org
Source0: https://archive.apache.org/dist/pulsar/pulsar-2.10.6/apache-pulsar-2.10.6-src.tar.gz
Source1: https://github.com/protocolbuffers/protobuf/archive/refs/tags/v3.19.6.tar.gz
Source2: https://github.com/grpc/grpc-java/archive/refs/tags/v1.45.1.tar.gz
Source3: https://services.gradle.org/distributions/gradle-7.3.3-bin.zip
Source4: https://github.com/google/protobuf/releases/download/v3.19.2/protobuf-all-3.19.2.tar.gz
Source5: https://github.com/protocolbuffers/protobuf/archive/refs/tags/v3.19.2.tar.gz

Patch0001: 0001-use-huawei-repository.patch
Patch0002: 0002-CVE-2022-22970.patch
Patch0003: 0003-CVE-2023-25194.patch
Patch0004: 0004-CVE-2023-2976.patch
Patch0005: 0005-CVE-2023-32732.patch

## %ifarch riscv64
Patch0006: 0006-Bump-maven-plugin-os-maven-plugin.patch
Patch0007: 0007-keep-gen-grpc-java-version.patch
Patch1000: 1000-Added-support-for-building-the-riscv64-protoc-binari.patch
Patch1001: 1001-Added-support-for-building-the-riscv64-protoc-gen-gr.patch
Patch1002: 1002-Add-Huawei-Maven-repository.patch
patch1003: 1003-Added-support-for-building-the-riscv64-protoc-binari.patch
## %endif

BuildRoot: /root/rpmbuild/BUILDROOT/
BuildRequires: java-1.8.0-openjdk-devel,maven,systemd
Requires: java-1.8.0-openjdk,systemd

%ifarch riscv64
BuildRequires: autoconf automake libtool pkgconfig zlib-devel libstdc++-static
%endif

Provides: apache-pulsar
Provides: mvn(org.apche.pulsar:pulsar)

%description
Pulsar is a distributed pub-sub messaging platform with a very flexible messaging model and an intuitive client API.

%prep
%ifarch riscv64
mkdir -p ${HOME}/%{name}-prep_dir
# protoc
tar -mxf %{SOURCE1} -C ${HOME}/%{name}-prep_dir
pushd ${HOME}/%{name}-prep_dir/protobuf-3.19.6
%patch1000 -p1
./autogen.sh
./protoc-artifacts/build-protoc.sh linux riscv64 protoc
mvn install:install-file -DgroupId=com.google.protobuf -DartifactId=protoc -Dversion=3.19.6 -Dclassifier=linux-riscv64 -Dpackaging=exe -Dfile=protoc-artifacts/target/linux/riscv64/protoc.exe
popd

# prepare gradle and protobuf for build protoc-gen-grpc-java
mkdir -p %{_tmppath}/source
cp %{SOURCE3} %{_tmppath}/source
tar xzf %{SOURCE4} -C %{_tmppath}/source
tar -mxf %{SOURCE5} -C ${HOME}/%{name}-prep_dir
pushd ${HOME}/%{name}-prep_dir/protobuf-3.19.2
%patch1003 -p1
./autogen.sh
./protoc-artifacts/build-protoc.sh linux riscv64 protoc
mvn install:install-file -DgroupId=com.google.protobuf -DartifactId=protoc -Dversion=3.19.2 -Dclassifier=linux-riscv64 -Dpackaging=exe -Dfile=protoc-artifacts/target/linux/riscv64/protoc.exe
popd

# protoc-gen-grpc-java
tar -mxf %{SOURCE2} -C ${HOME}/%{name}-prep_dir
pushd ${HOME}/%{name}-prep_dir/grpc-java-1.45.1
%patch1001 -p1
%patch1002 -p1
sed -i "s,@HOME@,${HOME},g" build.gradle
sed -i 's|https\\://services.gradle.org/distributions|file://%{_tmppath}/source|g' gradle/wrapper/gradle-wrapper.properties
SKIP_TESTS=true ARCH=riscv64 ./buildscripts/kokoro/unix.sh
mvn install:install-file -DgroupId=io.grpc -DartifactId=protoc-gen-grpc-java -Dversion=1.45.1 -Dclassifier=linux-riscv64 -Dpackaging=exe -Dfile=mvn-artifacts/io/grpc/protoc-gen-grpc-java/1.45.1/protoc-gen-grpc-java-1.45.1-linux-riscv64.exe
popd
%endif

%setup -q -n apache-pulsar-%{version}-src
%patch0001 -p1
%patch0002 -p1
%patch0003 -p1
%patch0004 -p1
%patch0005 -p1

%ifarch riscv64
%patch0006 -p1
%patch0007 -p1
%endif

%build
%ifarch riscv64
export MAVEN_OPTS="-Xms2048M -Xmx8000M"
mvn clean install -Pcore-modules,-main -DskipTests -Dspotbugs.timeout=7200000
%else
mvn clean install -Pcore-modules,-main -DskipTests
%endif

%install
mkdir -p $RPM_BUILD_ROOT%{_prefix}/{lib,bin,conf,examples,instances,licenses}
cd %{_builddir}/apache-pulsar-%{version}-src/distribution/server/target/
tar -xvf apache-pulsar-%{version}-bin.tar.gz
cd apache-pulsar-%{version}
cp -pr * $RPM_BUILD_ROOT%{_prefix}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%attr(-,pulsar,pulsar) %{_prefix}
%dir %attr(755, pulsar, pulsar) %{_prefix}

%pre
getent group pulsar >/dev/null || groupadd -r pulsar
getent passwd pulsar >/dev/null || useradd -r -g pulsar -d / -s /sbin/nologin pulsar
exit 0

%changelog
* Wed Dec 04 2024 yangzhenyu <dev11101@linx-info.com> - 2.10.6-3
- remove the architecture judgment in the patches section;
- include all patches in the source package.

* Mon Oct 14 2024 binshuo <binshuo.oerv@isrc.iscas.ac.cn> - 2.10.6-2
- Add support for riscv64 build

* Sun Apr 28 2024 dongjiao <dongjiao@kylinos.cn> - 2.10.6-1
- update to 2.10.6

* Fri Dec 8 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-19
- clean inactive bundle from bundleData in loadData and bundlesCache
* Fri Dec 8 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-18
- Return if AbstractDispatcherSingleActiveConsumer closed
* Fri Dec 8 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-17
- Fix return the earliest position when query position by timestamp. 
* Fri Dec 8 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-16
- Only handle exception when there has
* Thu Dec 7 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-15
- fix Can not receive any messages after switch to standby cluster
* Thu Dec 7 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-14
- resolve cve-2023-32732
* Thu Dec 7 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-13
- resolve fix deadlock
* Thu Dec 7 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-12
- resolve cve-2023-2976
* Wed Dec 6 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-11
- resolve cve-2023-25194
* Wed Dec 6 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-10
- resolve cve-2022-22970
* Mon Dec 5 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-9
- resolve cve-2022-24329
* Mon Dec 4 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-8
- resolve cve-2023-26048
* Mon Dec 4 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-7
- resolve cve-2022-1471
* Fri Dec 1 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-6
- fix memory leak
* Fri Dec 1 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-5
- resolve CVE-2023-34455
* Fri Dec 1 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-4
- upgrade netty to 4.1.89
* Mon Nov 27 2023 Dapeng Sun <sundapeng@cmss.chinamobile.com> - 2.10.4-3
- resolve CVE-2023-2976
* Thu Aug 24 2023 Jialing Wang <wangjialing@cmss.chinamobile.com> - 2.10.4-2
- resovle Cve-2023-32697
* Fri Aug 11 2023 Jialing Wang <wangjialing@cmss.chinamobile.com> - 2.10.4-1
- init puslar spec
